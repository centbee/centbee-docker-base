# Centbee base image (`FROM Ubuntu 18.04`)

### Pull newest build from Docker Hub
```
docker pull centbee/base:latest
```

### Run image
```
docker run -it centbee/base:latest bash
```

### Use as base image
```Dockerfile
FROM centbee/base:latest
```
