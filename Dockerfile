FROM ubuntu:bionic

LABEL maintainer="lorien@centbee.com" \
	org.label-schema.schema-version="1.0" \
	org.label-schema.name="Centbee Base Image" \
	org.label-schema.description="Base Image for all of Centbee images – base on Ubuntu 18.04." \
	org.label-schema.vendor="Lorien Gamaroff (Centbee)" \
	org.label-schema.license="MIT" 

ENV DEBIAN_FRONTEND=noninteractive \
	TERM=xterm